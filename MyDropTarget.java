import java.awt.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import java.lang.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.event.*;
import java.awt.image.*;
import javax.imageio.*;
import javax.swing.Timer;
import java.text.*;
import java.awt.dnd.*;
import java.awt.datatransfer.*;
import javax.swing.border.*;

class MyDropTarget implements DropTargetListener
    {
    PuzzleFrame tempFrame;
    JPanel mainPanel;
    JList listBox;
    DropTarget dropTarget;
    int count = 0;

//=============================================================
    public MyDropTarget(PuzzleFrame frame)
        {
        tempFrame = frame;
        listBox = new  JList();
        dropTarget = new DropTarget(frame, this);
        }
//=============================================================
    public void   dragEnter(DropTargetDragEvent dtde)
        {

        }
//=============================================================
    public void   dragExit(DropTargetEvent dte)
        {
        System.out.println("Exit");
        }
//=============================================================
//Checks the file type of objects over drop target
    public void   dragOver(DropTargetDragEvent dtde)
        {
        java.util.List<File> fileList;
        Transferable transferableData;
        DefaultListModel<String> dlm;
        int numElements;
        transferableData = dtde.getTransferable();
        try
    	    {
            if (transferableData.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
                {
                 fileList = (java.util.List<File>)(transferableData.getTransferData(DataFlavor.javaFileListFlavor));
                 dlm = new DefaultListModel<String>();
                 listBox.setModel(dlm);
                 PuzzleFrame.fileName = fileList.get(0).getPath();
                 numElements = fileList.size();
                 if(fileList.get(0).getPath().contains(".jpg") && numElements < 2)
                 	System.out.println("jpg");
                 else if(fileList.get(0).getPath().contains(".png") && numElements < 2)
                 	System.out.println("png");
                 else if(fileList.get(0).getPath().contains(".gif") && numElements < 2)
                 	System.out.println("gif");
                 else
                 	dtde.rejectDrag();
                 }
             else
             	{
                 System.out.println("File list flavor not supported.");
                 dtde.rejectDrag();
			 	}
             }
         catch (UnsupportedFlavorException  ufe)
             {
             System.out.println("Unsupported flavor found!");
             ufe.printStackTrace();
             }
         catch (IOException  ioe)
             {
             System.out.println("IOException found getting transferable data!");
             ioe.printStackTrace();
             }
        }
//=============================================================
//Changes the image being displayed to the one dropped on the frame
    public void   drop(DropTargetDropEvent dtde)
        {
        java.util.List<File>        fileList;
        Transferable                transferableData;
        DefaultListModel<String>    dlm;
        try
        	{
        	transferableData = dtde.getTransferable();
        	dtde.acceptDrop(DnDConstants.ACTION_COPY);
        	fileList = (java.util.List<File>)(transferableData.getTransferData(DataFlavor.javaFileListFlavor));
        	dlm = new DefaultListModel<String>();
        	listBox.setModel(dlm);
        	PuzzleFrame.fileName = fileList.get(0).getPath();
        	tempFrame.removePicture();
        	tempFrame.setupPicture();
			}
        catch (UnsupportedFlavorException  ufe)
			{
		    System.out.println("Unsupported flavor found!");
		    ufe.printStackTrace();
            }
         catch (IOException  ioe)
            {
            System.out.println("IOException found getting transferable data!");
            ioe.printStackTrace();
            }
        }

//=============================================================
    public void   dropActionChanged(DropTargetDragEvent dtde)
        {

        }
}
