import java.awt.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import java.lang.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.event.*;
import java.awt.image.*;
import javax.imageio.*;
import javax.swing.Timer;
import java.text.*;
import java.awt.dnd.*;
import java.awt.datatransfer.*;
import javax.swing.border.*;

public class Puzzle
    {
    public static void main(String args[])
        {
        new PuzzleFrame();
        }
    }
//===================================================================
class PuzzleFrame extends JFrame implements ActionListener
    {
    final int PUZZLE_SIZE = 5;
    static String fileName = "test.jpg";
    static String emptyImageName = "empty.jpg";
    BufferedImage fullImage;
    BufferedImage blankImage;
    ImageIcon missingPicturePiece;
    PuzzleButton buttonArray[][];
    int emptyRow;
    int emptyColumn;
    int numMoves;
    int incorrectPieces;
    Random randomMove;
    boolean isSolved;
    boolean firstMove;
    boolean isScrambling;
    boolean wasInCorrectPlace;
    JButton exitButton;
    JButton pauseResumeButton;
    JButton newImageButton;
    JButton clearButton;
    JButton scrambleButton;
    JLabel movesLabel;
    JLabel incorrectPiecesLabel;
    JPanel puzzlePanel;
    JPanel exitPanel;
    JPanel gameLabelsPanel;
    JPanel picturePanel;
    JFileChooser fileChooser;
    MyDropTarget dropTarget;
    TimeKeeperLabel playTimeLabel;

//=============================================================
    PuzzleFrame()
        {
        dropTarget = new MyDropTarget(this);
        fileChooser = new JFileChooser(".");
        incorrectPieces = 0;
        numMoves = 0;
        isSolved = false;
        firstMove = true;
        isScrambling = true;
        randomMove = new Random();
        addPanels();
        setupMainFrame();
        setupPicture();
        }
//====================================================================
//Divides and adds the subimages to the buttons
void setupPicture()
        {
        MediaTracker tracker;
        Image im;
        BufferedImage subImage;
        ImageIcon currentImageIcon;
        int width;
        int height;
        numMoves = 0;
        tracker = new MediaTracker(this);
        if(fileName != "test.jpg")
            {
            im = Toolkit.getDefaultToolkit().getImage(fileName);
            tracker.addImage(im,1);
            }
        emptyRow = PUZZLE_SIZE - 1;
        emptyColumn = PUZZLE_SIZE - 1;
        buttonArray = new PuzzleButton[PUZZLE_SIZE][PUZZLE_SIZE];
        picturePanel = new JPanel(new GridLayout(PUZZLE_SIZE,PUZZLE_SIZE));
        try
            {
            tracker.waitForID(1);
            if(tracker.isErrorID(1))
                throw (new ImageViewException("Image not constructed"));
            fullImage = ImageIO.read(new File(fileName));
            blankImage = ImageIO.read(new File(emptyImageName));
            width = fullImage.getWidth(this)/PUZZLE_SIZE;
            height = fullImage.getHeight(this)/PUZZLE_SIZE;
            for (int row = 0; row < PUZZLE_SIZE; row++)
                {
                for (int column = 0; column < PUZZLE_SIZE; column++)
                    {
                    subImage = fullImage.getSubimage(column*width, row*height,width,height);
                    tracker.addImage(subImage,row * PUZZLE_SIZE + column);
                    tracker.waitForID(row * PUZZLE_SIZE + column);
                    if(row < PUZZLE_SIZE-1 || column < PUZZLE_SIZE - 1)
                        currentImageIcon = new ImageIcon (subImage);
                    else
                        {
                        currentImageIcon = new ImageIcon(blankImage);
                        missingPicturePiece = new ImageIcon (subImage);
                        }
                    buttonArray[row][column] = new PuzzleButton(column,row,currentImageIcon);
                    buttonArray[row][column].addActionListener(this);
                    buttonArray[row][column].setBorder(new EmptyBorder(0,0,0,0));
                    picturePanel.add(buttonArray[row][column]);
                    }
                }
            tracker.waitForAll();
        	puzzlePanel.add(picturePanel);
        	getContentPane().add(puzzlePanel,BorderLayout.CENTER);
        	scramble();
            }
        catch (Exception ie)
            {
			playTimeLabel.stop();
            JOptionPane.showMessageDialog(this, "Error: file could not be opened");
            scrambleButton.setEnabled(false);
            repaint();
            }
        playTimeLabel.setText("  Time: 0.0");
        }
//===================================================================
//Shuffles the images on the buttons
    void scramble()
        {
        pauseResumeButton.setEnabled(false);
        int numShuffles;
        int direction;
        isSolved = false;
        firstMove = true;
        isScrambling = true;
        incorrectPieces = 0;
        numMoves = 0;
        numShuffles = PUZZLE_SIZE * PUZZLE_SIZE * PUZZLE_SIZE * PUZZLE_SIZE;
        for (int k = 0; k < numShuffles; k++)
            {
            direction = randomMove.nextInt(4);
            if(direction == 0)
                {
                if (emptyRow > 0)
                    buttonArray[emptyRow - 1][emptyColumn].doClick();
                }
            else if (direction == 1)
                {
                if (emptyRow < PUZZLE_SIZE - 1)
                    buttonArray[emptyRow + 1][emptyColumn].doClick();
                }
            else if (direction == 2)
                {
                if (emptyColumn > 0)
                    buttonArray[emptyRow][emptyColumn - 1].doClick();
                }
            else if (direction == 3)
                {
                if (emptyColumn < PUZZLE_SIZE - 1)
                    buttonArray[emptyRow][emptyColumn + 1].doClick();
                }
            }
        PuzzleButton nullButton;
        nullButton = new PuzzleButton(0,0,new ImageIcon(blankImage));
        findNumIncorrectPieces(nullButton);
        if(incorrectPieces == 0)
            scramble();
        incorrectPiecesLabel.setText("Number of incorrect pieces: " + incorrectPieces);
        movesLabel.setText("  Number of moves: " + numMoves);
        isScrambling = false;
        scrambleButton.setEnabled(true);
        }
//================================================================================
//Adds the buttons and panels to the GUI
    void addPanels()
        {
        pauseResumeButton = new JButton("Pause");
        clearButton = new JButton ("Clear");    //Just for timekeeper
        newImageButton = new JButton("New Image");
        scrambleButton = new JButton("Scramble Image");
        gameLabelsPanel = new JPanel ();
        movesLabel = new JLabel("  Number of moves: " + numMoves);
        incorrectPiecesLabel = new JLabel("Number of incorrect pieces: " + incorrectPieces);
        exitButton = new JButton("Exit");
        playTimeLabel = new TimeKeeperLabel(pauseResumeButton, clearButton);
        newImageButton.addActionListener(this);
        exitButton.addActionListener(this);
        pauseResumeButton.addActionListener(this);
        scrambleButton.addActionListener(this);
        exitPanel = new JPanel();
        exitPanel.add(pauseResumeButton);
        exitPanel.add(newImageButton);
        exitPanel.add(scrambleButton);
        exitPanel.add(exitButton);
        puzzlePanel = new JPanel();
        gameLabelsPanel.add(incorrectPiecesLabel);
        gameLabelsPanel.add(movesLabel);
        gameLabelsPanel.add(playTimeLabel);
        pauseResumeButton.setEnabled(false);
        getContentPane().add(exitPanel,BorderLayout.NORTH);
        getContentPane().add(gameLabelsPanel,BorderLayout.SOUTH);
        }
//=============================================================================
//Counts the number of pieces in the wrong position
    void findNumIncorrectPieces(PuzzleButton buttonToCheck)
        {
        if (isScrambling)
            {
            for (int row = 0; row < PUZZLE_SIZE; row++)
                {
                for (int column = 0; column < PUZZLE_SIZE; column++)
                    {
                    if (!buttonArray[row][column].inCorrectPlace())
                        incorrectPieces++;
                    }
                }
            }
        else
            {
            if(buttonToCheck.inCorrectPlace() && !wasInCorrectPlace)
                incorrectPieces--;
            else if(!buttonToCheck.inCorrectPlace() && wasInCorrectPlace)
                incorrectPieces++;
            }
        if(!buttonArray[emptyRow][emptyColumn].inCorrectPlace() && isScrambling)
            incorrectPieces--;
        }
//============================================================================
//Sets whether the buttons should be clickable
    void changePuzzleState(boolean currentState)
        {
        for (int row = 0; row < PUZZLE_SIZE; row++)
            {
            for (int column = 0; column < PUZZLE_SIZE; column++)
                {
                buttonArray[row][column].setEnabled(currentState);
                }
            }
        }
//============================================================================
//Sets the initial values for the JFrame
    void setupMainFrame()
        {
        Toolkit  tk;
        Dimension d;
        tk = Toolkit.getDefaultToolkit();
        d = tk.getScreenSize();
        setSize(d.width/2, d.height/2);
        setMinimumSize(new Dimension(d.width/3 , d.height/3));
        setLocation(d.width/4,d.height/4);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Puzzle");
        setVisible(true);
        }
//============================================================================
//Handles user interaction with the buttons
    public void actionPerformed(ActionEvent e)
        {
        if (e.getSource() == exitButton)
            System.exit(0);
        else if(e.getSource() == newImageButton)
            loadNewImage();
        else if (e.getSource() == pauseResumeButton && !playTimeLabel.isPaused && missingPicturePiece != buttonArray[emptyRow][emptyColumn].getIcon())
            {
			changePuzzleState(false);
			scrambleButton.setEnabled(false);
			}
        else if (e.getSource() == pauseResumeButton && playTimeLabel.isPaused)
        	{
            changePuzzleState(true);
            scrambleButton.setEnabled(true);
			}
        else if(e.getSource() == scrambleButton)
            {
            playTimeLabel.stop();
            clearButton.doClick();
            scramble();
            }
        else if(e.getSource()!= pauseResumeButton)
            {
            PuzzleButton tempButton;
            ImageIcon tempIcon;
            tempButton = (PuzzleButton)e.getSource();
            if(tempButton.isAdjacentToEmpty(emptyRow,emptyColumn)&& missingPicturePiece != buttonArray[emptyRow][emptyColumn].getIcon())
                {
                int newRow = emptyRow;
                int newColumn = emptyColumn;
                wasInCorrectPlace = tempButton.inCorrectPlace();
                move(tempButton, buttonArray[emptyRow][emptyColumn]);
                if (!isScrambling)
                    {
                    pauseResumeButton.setEnabled(true);
                    tempButton = buttonArray[newRow][newColumn];
                    findNumIncorrectPieces(tempButton);
                    numMoves++;
                    movesLabel.setText("  Number Of moves: "+ numMoves);
                    incorrectPiecesLabel.setText("Number of incorrect pieces: " + incorrectPieces);
                    System.out.println(incorrectPieces);
                    if(incorrectPieces == 0)
                        {
                        buttonArray[emptyRow][emptyColumn].setIcon(missingPicturePiece);
                        pauseResumeButton.doClick();
                        JOptionPane.showMessageDialog(this, "Congratulations");
                        pauseResumeButton.setEnabled(false);
                        scrambleButton.setEnabled(false);
                        }
                    if(firstMove && missingPicturePiece != buttonArray[emptyRow][emptyColumn].getIcon())
                        {
                        playTimeLabel.start();
                        firstMove = false;
                        }
                    }
                }
            }
        }
//======================================================================================
//Switches the image of the button to move to the button that is "blank"
    void move(PuzzleButton adjacentButton, PuzzleButton emptyButton)
        {
        ImageIcon tempIcon;
        tempIcon = (ImageIcon)adjacentButton.getIcon();
        adjacentButton.setIcon(emptyButton.getIcon());
        emptyButton.setIcon(tempIcon);
        emptyRow = adjacentButton.row;
        emptyColumn = adjacentButton.column;
        }
//====================================================================================
//Loads a new image for the puzzle
    void loadNewImage()
        {
        int returnValue;
        if(pauseResumeButton.getText() == "Pause")
        	pauseResumeButton.doClick();
        returnValue = fileChooser.showOpenDialog(picturePanel);
        if (validFile(returnValue))
            {
            fileName = fileChooser.getCurrentDirectory() + "/" + fileChooser.getSelectedFile().getName();
            removePicture();
            setupPicture();
            }
        else
        	pauseResumeButton.doClick();
        }
//========================================================================================
//Removes the puzzle from the JFrame
	void removePicture()
		{
		puzzlePanel.remove(picturePanel);
        getContentPane().remove(picturePanel);
		}
//========================================================================================
    boolean validFile(int x)
        {
        return x == JFileChooser.APPROVE_OPTION;
        }
    }
//###########################################################################################
class ImageViewException extends Exception
    {
    ImageViewException(String msg)
        {
        super(msg);
        }
    }
