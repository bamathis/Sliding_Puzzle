import java.awt.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import java.lang.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.event.*;
import java.awt.image.*;
import javax.imageio.*;
import javax.swing.Timer;
import java.text.*;
import java.awt.dnd.*;
import java.awt.datatransfer.*;
import javax.swing.border.*;

class TimeKeeperLabel extends JLabel implements ActionListener
    {
    Timer updateTime;
    long startTime;
    double timeRun;
    double displayTime;
    DecimalFormat timeFormatter;
    static boolean isPaused;
    JButton pauseResumeButton;
    JButton clearButton;

//=====================================================================
    TimeKeeperLabel(JButton tempPauseResumeButton, JButton tempClearButton)
        {
        tempPauseResumeButton.addActionListener(this);
        tempClearButton.addActionListener(this);
        pauseResumeButton = tempPauseResumeButton;
        clearButton = tempClearButton;
        isPaused = false;
        updateTime = new Timer(200,this);
        timeFormatter = new DecimalFormat("###0.0");
        setText("  Time: 0.0");
        }
//=====================================================================
//Starts the timer and resets current time values
    void start()
        {
        isPaused = false;
        timeRun = 0;
        startTime = System.currentTimeMillis();
        pauseResumeButton.setText("Pause");
        updateTime.start();
        }
//====================================================================
//Stops the timer
    void stop()
        {
        pauseResumeButton.setText("Resume");
        updateTime.stop();
        }
//====================================================================
//Handles timer events and user interaction with pauseResumeButton
    public void actionPerformed(ActionEvent   e)
        {
        if (e.getSource() == updateTime && !isPaused)
            processUpdateTime();
        else if (e.getSource() == pauseResumeButton && !isPaused) //timer is currently running
            processPauseButton();
        else if (e.getSource() == pauseResumeButton && isPaused) //timer is currently paused
            processResumeButton();
        else if (e.getSource() == clearButton)
            processClearButton();
        }
//=======================================================================
//Updates the display of the timer
    void processUpdateTime()
        {
        displayTime = (timeRun + System.currentTimeMillis() - startTime) * .001;
        setText("  Time: "+ timeFormatter.format(displayTime));
        }
//========================================================================
//Resets the timer
    void processClearButton()
        {
        isPaused = true;
        timeRun = 0;
        pauseResumeButton.setText("Pause");
        setText("  Time: 0.0");
        }
//=========================================================================
//Pauses the timer
    void processPauseButton()
        {
        isPaused = true;
        timeRun = timeRun + System.currentTimeMillis() - startTime;
        pauseResumeButton.setText("Resume");
        setText("  Time: " + timeFormatter.format(displayTime));
        }
//==========================================================================
//Resumes the timer
    void processResumeButton()
        {
        isPaused = false;
        startTime = System.currentTimeMillis();
        pauseResumeButton.setText("Pause");
        }
    }