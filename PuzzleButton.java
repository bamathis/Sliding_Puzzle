import java.awt.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import java.lang.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.event.*;
import java.awt.image.*;
import javax.imageio.*;
import javax.swing.Timer;
import java.text.*;
import java.awt.dnd.*;
import java.awt.datatransfer.*;
import javax.swing.border.*;

class PuzzleButton extends JButton
    {
    int column;
    int row;
    ImageIcon correctImage;

//=====================================================================================
    PuzzleButton(int tempColumn, int tempRow,ImageIcon buttonImage)
        {
        super(buttonImage);
        column = tempColumn;
        row = tempRow;
        setIcon(buttonImage);
        correctImage = buttonImage;
        }
//=====================================================================================
//Checks whether the button can be moved into the empty position
    boolean isAdjacentToEmpty(int emptyRow, int emptyColumn)
        {
        if (column == emptyColumn)
            {
            if(row == emptyRow + 1)
                return true;
            else if (row == emptyRow - 1)
                return true;
            }
        else if (row == emptyRow)
            {
            if(column == emptyColumn + 1)
                return true;
            else if (column == emptyColumn - 1)
                return true;
            }
        return false;
        }
//=========================================================================================
//Checks whether the button is where it is supposed to be
    boolean inCorrectPlace()
        {
        return getIcon()== correctImage;
        }
    }