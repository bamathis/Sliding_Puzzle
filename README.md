# Sliding_Puzzle

[Sliding_Puzzle](https://gitlab.com/bamathis/Sliding_Puzzle) is an implementation of sliding puzzle game in Java.

## Quick Start

### Dependencies

- Java 7+

### Program Execution

```
$ java Puzzle
```

### Input Files

Initially reads the files **test.jpg** and **empty.jpg** in the current working directory to create the first puzzle.
